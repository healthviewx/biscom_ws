__author__ = 'sriraman.v'

"""
Setuptools for Biscom Webservices

"""

from setuptools import setup, find_packages


setup(
    name='biscom_ws',
    description='Biscom Webservices',
    version='0.0.1',
    packages=find_packages(),
    zip_safe=False,
    url='https://gitlab.com/healthviewx/biscom_ws.git',
    author='Sriraman vellingiri',
    author_email='sriraman.v@payoda.com',
    license='Apache 2.0',
    install_requires=[],
)
