import base64,suds,logging
from suds.client import Client
logging=logging.getLogger(__name__)

class BiscomWS(object):
    """
    Biscom Server Webservice API class. 
    Compatible with version 10.x
    """
    def __init__(self,webservice_url,session_id = None):
        """
        Set client object
        params: Webservice URL,<session_id>
        """
        logging.info('Webservice URL: %s',webservice_url)
        try:
            self.client_obj = Client(url=webservice_url)        
            self.client_obj.set_options(location=webservice_url)
            if session_id is not None:
                logging.debug('Existing session id: %s',session_id)
                self.client_obj = self.set_session(self.client_obj, webservice_url,session_id)
            logging.info('Session id: %s',session_id)
            # logging.debug('Client object: %s',self.client_obj)
        except Exception, e:
            return self.error_handler(e)

    def get_domain_name(self, url):
        """
        Extract domain name from the Webservice URL
        param: Webservice url
        return: domain name
        """
        try:
            logging.info('Extracting domain name for the URL %s',url)
            spltAr = url.split("://");
            i = (0, 1)[len(spltAr) > 1];
            domain_name = spltAr[i].split("?")[0].split('/')[0].split(':')[0].lower();
            logging.debug('%s is the domain name for the URL %s',domain_name,url)
            return domain_name
        except Exception, e:
            return self.error_handler(e)

    def set_session(self, fax_obj, url, session_id):
        """
        Set session information in the client object
        params: fax_obj, Webservice Url, session_id
        return: session
        """
        try:
            from cookielib import Cookie, CookieJar
            cookie_jar = CookieJar()

            domain_name = self.get_domain_name(url)
            cookie_name = 'ASP.NET_SessionId'
            
            # More information on Cookie object - https://docs.python.org/2/library/cookielib.html#cookie-objects
            cookie_info = Cookie(0, cookie_name, session_id, None, False, domain_name, None, None,
                                 '/', True, False, None, True, None, None, {'HttpOnly': None}, None)
            logging.info('Cookie information %s',cookie_info)
            cookie_jar.set_cookie(cookie_info)
            fax_obj.options.transport.cookiejar = cookie_jar
            # logging.debug('Fax Object %s',fax_obj) #faxobject is large
            return fax_obj
        except Exception, e:
            return self.error_handler(e)

    def get_session(self, session_data):
        """
        Get session information from the cookie/Session data
        param: session data
        return: session id
        """
        try:
            session_id = None
            logging.info('Getting session id from %s session data',session_data)
            for info in session_data:
                session_id = info.value
            logging.debug('Session id: %s',session_id)
            return session_id
        except Exception, e:
            return self.error_handler(e)

    def parse_response(self, response_obj):
        """
        Parsing response from request
        param: response_obj
        return response
        """
        response = {}
        try:
            logging.info('Parsing response')
            logging.debug('Parsing response object: %s',response_obj)

            if "Result" in response_obj: # Used for login ,logout ,send_token
                if response_obj['Result'] == True:
                    response = {'status': True, 'type': response_obj['Type'], 'details': response_obj['Detail']}
                    if "Data" in response_obj:
                        response.update({'data' :response_obj['Data']})
                elif response_obj['Result'] == False:
                    response = {'status': False, 'type': response_obj['Type'], 'details': response_obj['Detail']}
            elif "ActivityRecord" in response_obj: # Used for get_job_activity
                return {'status': True,  'details': response_obj['ActivityRecord']}
            elif "PendingMessage" in response_obj: # Used for get Pending message by ID
                return {'status': True,  'details': response_obj['PendingMessage']}
            elif "MessageStatusQueryResult" in response_obj:  # Used for get_messsage_status_by_recipient
                return {'status': True, 'details': response_obj['MessageStatusQueryResult']}
            elif "MessageStatus" in response_obj:  # Used for get_messsage
                return {'status': True, 'details': response_obj['MessageStatus']}
            else:
                response = {'status': False, 'details': response_obj}
            logging.debug('Final response object: %s',response)
            return response
        except Exception, e:
            return self.error_handler(e)

    def login(self, queue, user_name, password, user_type):
        """
        Biscom Login
        params: queue, user_name, password, user_type
        return: session id
        """
        try:
            logging.info('Initiate Login ...')
            logging.debug('Login details Q:%s, Username:%s, Usertype:%s',queue, user_name, user_type)
            response = self.parse_response(self.client_obj.service.LogOn(queue, user_name, password, user_type))
            if response['status']:
                session_id = self.get_session(self.client_obj.options.transport.cookiejar)
                logging.debug('Getting session id on login: %s',session_id)
                return {'status' : True ,'data' : {'session_id': session_id}}
            return response
        except suds.WebFault as detail:
            return self.webservice_failed(detail)
        except Exception, e:
            return self.error_handler(e)

    def refresh_session(self):
        """
        Refresh Session to keep the session active
        return: response
        """
        try:
            logging.info('Refresh Session ...')
            response = self.parse_response(self.client_obj.service.GetLastUniqueJobID())
            logging.debug('Getting last unique job id: %s',response)
            return response
        except suds.WebFault as detail:
            return self.webservice_failed(detail)
        except Exception, e:
            return self.error_handler(e)

    def get_message_status(self,job_id):
        """
        Get message status for a job id
        param: job_id
        return: response
        """
        try:
            logging.info('Message Status for a Job id ...: %s',job_id)
            response = self.client_obj.service.GetMessageStatusByUniqueID(job_id)
            logging.debug('Getting Message Status for a Job id : %s', response)
            return response
        except suds.WebFault as detail:
            return self.webservice_failed(detail)
        except Exception, e:
            return self.error_handler(e)

    def get_job_activity(self, job_id):
        """
        Get Job activity for a job id
        param: job_id
        return: response
        """
        try:
            logging.info('Job activity for a Job id ...: %s', job_id)
            response = self.parse_response(self.client_obj.service.GetJobActivity(job_id))
            logging.debug('Getting Job activity for a Job id : %s', response)
            return response
        except suds.WebFault as detail:
            return self.webservice_failed(detail)
        except Exception, e:
            return self.error_handler(e)

    def new_fax_message(self, job_id,recipient_id=0):
        """
        New Fax Message
        param: job_id,recipient_id
        return: response
        """
        try:
            logging.info('Creating New fax message  from the Job id...: %s', job_id)
            response = self.parse_response(self.client_obj.service.NewFaxMessageFromUniqueID(job_id,recipient_id))
            logging.debug('Created New fax message for a Job id : %s', response)
            return response
        except suds.WebFault as detail:
            return self.webservice_failed(detail)
        except Exception, e:
            return self.error_handler(e)

    def get_fax_message(self):
        """
        Get Fax Message based on newFaxmessage
        return: response
        """
        try:
            response = self.client_obj.service.GetFaxMessageProperties()
            logging.debug('Getting Job activity for a Job id : %s', response)
            return response
        except suds.WebFault as detail:
            return self.webservice_failed(detail)
        except Exception, e:
            return self.error_handler(e)

    def set_fax_message(self, message_properties):
        """
        Set Fax message
        param: message_properties
        return: response
        """
        try:
            logging.debug('Setting Fax Message with Message properities %s', message_properties)
            response = self.parse_response(self.client_obj.service.SetFaxMessageProperties(message_properties))
            logging.debug('Setting Fax Message : %s', response)
            return response
        except suds.WebFault as detail:
            return self.webservice_failed(detail)
        except Exception, e:
            return self.error_handler(e)

    def send_fax(self):
        """
        Get Job activity for a job id
        param: job_id
        return: response
        """
        try:
            logging.info('Send fax')
            response = self.parse_response(self.client_obj.service.SendFax())
            logging.debug('Send fax response: %s', response)
            return response
        except suds.WebFault as detail:
            return self.webservice_failed(detail)
        except Exception, e:
            return self.error_handler(e)


    def set_sender(self, sender_data):
        """
        Set Sender data
        param: sender data
        return: sender_obj
        """
        sender_data_obj = ''
        logging.info('Setting sender object ...')
        sender_data_obj = self.client_obj.factory.create('SenderInfo')
        sender_data_obj.Name = sender_data['name']
        sender_data_obj.Company = sender_data['company']
        sender_data_obj.FaxNumber = sender_data['fax_no']
        logging.debug('Sender object: %s', sender_data_obj)
        return sender_data_obj


    def set_recipient(self,recipient_data):
        """
        Set Recipient data
        param: recipient data
        return: recipient_obj
        """
        logging.info('Setting recipient object ...')
        recipients = []
        for data in recipient_data:
            recipient_info_obj = self.client_obj.factory.create('RecipientInfo')
            recipient_info_obj.Name = data['name']  # "HealthviewX"
            recipient_info_obj.Company = data['company']
            recipient_info_obj.FaxNumber = data['fax_no']
            recipients.append(recipient_info_obj)

        if len(recipients) == 1:
            recipient_info_obj = recipients[0]
        else:
            recipient_info_obj = recipients

        logging.debug('Recipient object: %s',recipient_info_obj)
        return recipient_info_obj

    def frame_attachment_data(self, attachment_data):
        """
        Frame Attachment data
        param: attachment_data
        return: attachment_obj
        """
        logging.info('Setting attachment object ...')
        attachments = []
        for data in attachment_data:
            attachment_obj = self.client_obj.factory.create('Attachment')
            attachment_obj.FileName = data['file_name']
            attachment_obj.FileContent = base64.b64encode(data['file_content'])
            attachments.append(attachment_obj)

        if len(attachments) == 1: # if the attachment is one , first element of attachment will be assigned to the attachment_obj
            attachment_obj = attachments[0]
        else:
            attachment_obj = attachments

        #logging.debug('Attachment object: %s',attachment_obj) # Log file will be large for large PDF files
        return attachment_obj

    def send_new_fax(self,fax_details,sender,recipients,attachments):
        """
        Send Fax
        params: fax_details,sender,recipients, attachments
        return: response
        """
        try:
            logging.info('Sending New fax ...')
            subject = fax_details['subject']
            memo = fax_details['memo']

            sender_factory, recipient_factory, attachment_factory = '','',''

            if sender is not None:
                sender_factory = self.set_sender(sender)

            if recipients is not None:
                recipient_factory = self.client_obj.factory.create('ArrayOfRecipientInfo')
                recipient_factory.RecipientInfo = self.set_recipient(recipients)

            if attachments is not None:
                attachment_factory = self.client_obj.factory.create('ArrayOfAttachment')
                attachment_factory.Attachment =  self.frame_attachment_data(attachments)

            logging.debug('Fax details object: %s',fax_details)

            response = self.parse_response(self.client_obj.service.SendNewFaxMessage("",2,"0.0",1,subject,"",memo,sender_factory,recipient_factory,attachment_factory,""))

            logging.debug('Send new fax response: %s',response)
            logging.info('Fax sent.')
            return response
        except suds.WebFault as detail:
            return self.webservice_failed(detail)
        except Exception, e:
            import traceback
            traceback.print_exc()
            return self.error_handler(e)


    def resend_failed_fax(self, job_id , recipient_index ,fax_no , send_time='0.0', priority=2):
        """
        Resend Failed Fax
        params: job_id,recipient_index,fax_no,send_time,priority
        return: response
        """
        try:
            logging.info('Resend failed Fax ...')
            response = self.parse_response(self.client_obj.service.ResendFailedFax(job_id , recipient_index ,fax_no ,send_time,priority))
            logging.debug('Resend failed Fax: %s', response)
            logging.info('Resend failed Fax.')
            return response
        except suds.WebFault as detail:
            return self.webservice_failed(detail)
        except Exception, e:
            return self.error_handler(e)


    def get_message_status(self, sort_coloumn= 2 , ascending= True):
        """
        Get Message Status
        params: job_id,recipient_index,fax_no,send_time,priority
        return: response
        """
        try:
            logging.info('Get Message Status...')
            response = self.parse_response(self.client_obj.service.GetMessageStatuses(sort_coloumn , ascending))
            logging.debug('Get Message Status: %s', response)
            logging.info('Get Message Status.')
            return response
        except suds.WebFault as detail:
            return self.webservice_failed(detail)
        except Exception, e:
            return self.error_handler(e)

    def get_pending_message_status(self, sort_coloumn=2, ascending=True):
        """
        Get Message Status
        params: job_id,recipient_index,fax_no,send_time,priority
        return: response
        """
        try:
            logging.info('Get Message Status...')
            response = self.parse_response(self.client_obj.service.GetPendingMessages(sort_coloumn, ascending))
            logging.debug('Get Message Status: %s', response)
            logging.info('Get Message Status.')
            return response
        except suds.WebFault as detail:
            return self.webservice_failed(detail)
        except Exception, e:
            return self.error_handler(e)

    def cancel_pending_messsage(self, job_id,recipient_index=1):
        """
        Cancel Pending Message
        params: job_id,recipient_index
        return: response
        """
        try:
            logging.info('Cancel pending Message...')
            response = self.parse_response(self.client_obj.service.CancelPendingMessage(job_id,recipient_index))
            logging.debug('Cancel pending Message: %s', response)
            logging.info('Cancel pending Message.')
            return response
        except suds.WebFault as detail:
            return self.webservice_failed(detail)
        except Exception, e:
            return self.error_handler(e)

    def get_pending_messsage_by_recipient(self,job_id,recipient_index = 1):
        """
        Get Pending Message
        params: job_id,recipient_index
        return: response
        """
        try:
            logging.info('Get pending Message...')
            response = self.parse_response(self.client_obj.service.GetPendingMessageByUniqueID(job_id,recipient_index))
            logging.debug('Get pending Message: %s', response)
            logging.info('Get pending Message.')
            return response
        except suds.WebFault as detail:
            return self.webservice_failed(detail)
        except Exception, e:
            return self.error_handler(e)

    def get_messsage_status_by_recipient(self, job_id, recipient_index=0):
        """
        Get Message status by recipients
        params: job_id,recipient_index
        return: response
        """
        try:
            logging.info('Get Message status by recipient...')
            message_status_query = self.client_obj.factory.create('ArrayOfMessageStatusQuery')
            message_status = self.client_obj.factory.create('MessageStatusQuery')
            message_status.UniqueID = job_id
            message_status.Index = recipient_index
            message_status_query.MessageStatusQuery.append(message_status)
            response = self.parse_response(self.client_obj.service.GetMessageStatusesByMessageStatusQueries(message_status_query))
            logging.debug('Get Message status by recipient: %s', response)
            logging.info('Get Message status by recipient.')
            return response
        except suds.WebFault as detail:
            return self.webservice_failed(detail)
        except Exception, e:
            return self.error_handler(e)

    def logout(self):
        """
        Logout - Clears session
        return: response
        """
        try:
            logging.info('Logging out ...')
            response = self.parse_response(self.client_obj.service.ReleaseSession())
            logging.debug('Logout response: %s',response)
            return response
        except suds.WebFault as detail:
            return self.webservice_failed(detail)
        except Exception, e:
            return self.error_handler(e)

    def get_webservice_version(self):
        """
        Webservice Version of Biscom
        return: response
        """
        try:
            response = self.client_obj.service.WebServiceVersion()
            return response
        except suds.WebFault as detail:
            return self.webservice_failed(detail)
        except Exception, e:
            return self.error_handler(e)
    
    def error_handler(self,err):
        """
        Error handler
        """
        error = {'status': False, 'details': err} 
        logging.error('Exception occured due to : %s',error)
        return error

    def webservice_failed(self,detail):
        """
        Webservice Failure handler
        """
        details = {'status': False, 'details': str(detail)}
        logging.error('Webservice returned failed response: %s',details)
        return details
 